@extends('layouts.app')
@section('title', 'Customer')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Customer List</h1>
                </div>
            </div>

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Status</th>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($customerList as $key => $man)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $man->name }}</td>
                                                <td>{{ $man->phone_no }}</td>

                                                <td>
                                                    <label href="#" class="badge badge-info">
                                                        @if ($man->status == 1) Active
                                                        @else Inactive
                                                        @endif
                                                    </label>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info"
                                                        href="{{ route('customer.detailsCustomer', $man->id) }}">
                                                        View
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>

@endsection

@section('extra-js')

@endsection
