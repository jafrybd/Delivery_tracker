@extends('layouts.app')
@section('title', 'Customer')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Order History</h1>
            </div>

        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Customer</th>
                                        <th>Delivery Man</th>
                                        <th>Product Category</th>
                                        <!-- <th>Weight</th> -->
                                        <th>Status</th>
                                        <th>Order Date</th>
                                        <th>Delivered To</th>
                                        <th>Contact Number</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orderHistory as $key => $order)

                                    <?php
                                    $status = "";
                                    if ($order->order_status == -1) {
                                        $status = "Reject";
                                    } else if ($order->order_status == 0) {
                                        $status = "Pending";
                                    } else if ($order->order_status == 1) {
                                        $status = "Accept";
                                    } else if ($order->order_status == 2) {
                                        $status = "Receive";
                                    } else if ($order->order_status == 3) {
                                        $status = "On the way";
                                    } else if ($order->order_status == 5) {
                                        $status = "Delivered";
                                    }

                                    ?>
                                    <tr>
                                        <td>{{ $key + 1 }}</td>

                                        <td> <a href="{{ route('customer.detailsCustomer', $order->customer->id) }}"> {{ $order->customer->name }} </a> </td>

                                        @if($order->deliveryMan_id == null)
                                        <td>Not Assign</td>
                                        @else
                                        <td> <a href="{{ route('delivery-man.details', $order->deliveryMan->id) }}"> {{ $order->deliveryMan->name }} </a> </td>

                                        @endif

                                        <td>{{ $order->product_category }}</td>

                                        <!-- <td>{{ $order->product_weight }}</td> -->

                                        <td>{{ $status}}</td>


                                        <td>{{ \Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}</td>
                                        <td>{{ $order->delivered_to }}</td>
                                        <td>{{ $order->contact_number }}</td>

                                        <td>
                                            <a href="{{ route('order.order-details-admin', $order->id) }}" class="btn btn-primary"> <i class="far fa-eye"></i> </a>
                                        </td>
                                    </tr>


                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>




@endsection

@section('extra-js')

<script>
    function changeModalData(id = 0) {
        document.getElementById('model_id').value = id;
    }
</script>

@endsection