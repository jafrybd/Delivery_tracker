<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained('users');
            $table->foreignId('deliveryMan_id')->nullable()->constrained('users');
            $table->string('product_category');
            $table->integer('product_weight');
            $table->string('delivered_to');
            $table->string('pickup_address');
            $table->string('destination_address');
            $table->string('contact_number');
            $table->double('amount', 13, 2)->default(0);
            $table->integer('order_status')->default(0); // -1 = reject,  0 = Pending , 1 = Accept , 2 = receive, 3 = on the way ,.... 5 = deliverd 
            $table->boolean('active_status')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
