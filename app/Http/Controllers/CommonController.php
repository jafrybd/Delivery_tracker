<?php

namespace App\Http\Controllers;


use \PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonController extends Controller
{

  public function getMonthsName()
  {
    $monthsName = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    return $monthsName;
  }

}
