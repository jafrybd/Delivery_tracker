<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \PDF;
use Validator;
use App\Models\OrderActionStatus;
use CreateOrderActionStatus;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    //
    public function index()
    {
        $totalCompletedOrders = Orders::where([['customer_id' , '=',Auth::user()->id],['order_status', '=', 5], ['active_status', '=', 1]])->count();
         
        $totalRunningOrders = Orders::where([['customer_id' , '=',Auth::user()->id],['active_status', '=', 1]])->whereIn('order_status', [1,2,3])->count();
        
        $totalPendingOrders = Orders::where([['customer_id' , '=',Auth::user()->id],['order_status', '=', 0], ['active_status', '=', 1]])->count();

        $runningOrderList = Orders::where([
            ['customer_id', '=', Auth::user()->id],
            ['order_status', '!=', 5],
            ['active_status', "!=", 0]
        ])->orderBy('id', 'DESC')->limit(5)->get();
        return view('customer.customerDashboard',compact('totalCompletedOrders','totalRunningOrders','totalPendingOrders','runningOrderList'));
    }

    public function list()
    {
        
        $customerList = User::where([['role_id', '=', 3]])->get();
        
        return view('customer.list',compact('customerList'));
    }

    public function detailsCustomer($id)
    {
        
        $customer = User::findorFail($id);
        $runningOrderList = Orders::where([['customer_id', '=', $id]])->orderBy('id', 'DESC')->get();

        $complete_order = 0;
        $running_order = 0;
        $cancel_order = 0;

        for ($i=0; $i < count($runningOrderList); $i++) { 
            if ($runningOrderList[$i]->order_status == 5) $complete_order++;
            else if ($runningOrderList[$i]->order_status == -1) $cancel_order++;
            else if ($runningOrderList[$i]->order_status > 0  && $runningOrderList[$i]->order_status < 5)  $running_order++;
        }

        return view('customer.details', compact('customer', 'id', 'runningOrderList', 'cancel_order', 'complete_order', 'running_order' ));
    }

    
}
